There are several items in this directory, including:
* verify_lists_description.txt - description of requirements for verify_lists implementation
* verify_lists.rb - implementation of verify_lists in Ruby
* verify_lists.py - implementation of verify_lists in Python
* verify_lists_test.py - unit tests for verification_lists.py
* compare_data.py - script to compare and report on similarity of two tables
* test_server.py - web server front end for test services; depends on web.py and jinja2
* requirements.txt - pip dependency manifest for web server