
require 'set'
require 'logger'
$LOG = Logger.new('verify_lists_rb.log')


def verify_lists(list1, list2, qualifier)
    list1_items = list1.split(';')
    list2_items = list2.split(';')
    
    if qualifier == "contains" then
        return _verify_lists_contains(list1_items, list2_items)

    elsif qualifier == "only contains" then
        return _verify_lists_only_contains(list1_items, list2_items)

    elsif qualifier == "contains in order" then
        return _verify_lists_contains_in_order(list1_items, list2_items)

    elsif qualifier == "does not contain" then
        return _verify_lists_does_not_contain(list1_items, list2_items)
    
    else
        raise Exception('invalid qualifier for verify_lists', qualifier)
	end
end

def _verify_lists_contains_in_order(list1_items, list2_items)
    if list1_items.length < list2_items.length then
        $LOG.info("Fail (contains in order): Only %s items in expected list '%s' to compare to %s items in actual list '%s'" %
                     [list1_items.length, list1_items.join(';'), list2_items.length, list2_items.join(';')])
        return false
    end
        
    # different behavior than Python... in the zip method, shorter lists are
    # padded with nil to make them the same length as the longer one
    min_len = list1_items.length
    if list2_items.length < min_len then
    	min_len = list2_items.length
    end
    list1_items_trim = list1_items[0, min_len]
    list2_items_trim = list2_items[0, min_len]
    verify = true
    for list1_item, list2_item in list1_items_trim.zip(list2_items_trim)
        if list1_item != list2_item then
            verify = false
            break
        end
    end

    if verify then
        $LOG.info("Pass (contains in order): Verified expected list '%s' contains in order actual list '%s'" %
                     [list1_items.join(';'), list2_items.join(';')])
        return true
    else
        $LOG.info("Fail (contains in order): Order of items in expected list '%s' does not match order of items in actual list '%s'" %
                     [list1_items.join(';'), list2_items.join(';')])
        return false
    end
end


def _verify_lists_does_not_contain(list1_items, list2_items)
    list1_set = Set.new(list1_items)
    list2_set = Set.new(list2_items)
    if list1_set.intersection(list2_set).length == 0 then
        $LOG.info("Pass (does not contain): Verified expected list '%s' does not contain actual list '%s'" %
                     [list1_items.join(';'), list2_items.join(';')])
        return true
    else
        intersection = list1_set.intersection(list2_set).to_a
        $LOG.info("Fail (does not contain): Expected list '%s' contains '%s' and should NOT [actual list is '%s']" %
                     [list1_items.join(';'), intersection.join(';'), list2_items.join(';')])
        return false
    end
end


def _verify_lists_only_contains(list1_items, list2_items)
    list1_set = Set.new(list1_items)
    list2_set = Set.new(list2_items)
    if list1_set == list2_set then
        $LOG.info("Pass (only contains): Verified list %s contains %s" %
                     [list1_items.join(';'), list2_items.join(';')])
        return true
    else
        missing_items = list1_set.difference(list2_set).to_a
        extra_items = list2_set.difference(list1_set).to_a
        $LOG.info("Fail (only contains): Expected list '%s' is MISSING '%s' and actual list contains extras '%s' [actual list is '%s']" %
                     [list1_items.join(';'), missing_items.join(';'),
                      extra_items.join(';'), list2_items.join(';')])
        return false
    end
end


def _verify_lists_contains(list1_items, list2_items)
    list1_set = Set.new(list1_items)
    list2_set = Set.new(list2_items)
    if list2_set.proper_subset? list1_set then
        $LOG.info("Pass (contains): Verified expected list '%s' contains '%s'" %
                     [list1_items.join(';'), list2_items.join(';')])
        return true
    else
        missing_items = list2_set.difference(list1_set).to_a
        $LOG.info("Fail (contains): Expected list '%s' does NOT contain '%s' [actual list is '%s']" %
                     [list1_items.join(';'), missing_items.join(';'), list2_items.join(';')])
        return false
    end
end



verify_lists("A;B;C;D", "A;D;C", "contains")
verify_lists("A;B;C;D", "A;D;C;E", "contains")
verify_lists("A;B;C;D", "C;D;A", "contains")
verify_lists("A;B;C;D", "A", "contains")
verify_lists("", "A;D;C;E", "contains")

verify_lists("A;B;C;D", "A", "does not contain")
verify_lists("A;B;C;D", "A;D;C;E", "does not contain")
verify_lists("A;B;C;D", "1;2;9", "does not contain")
verify_lists("A;B;C;D", "x", "does not contain")
 
verify_lists("A;B;C;D", "A", "only contains")
verify_lists("A;B;C;D", "A;D;C;E", "only contains")
verify_lists("A;B;C;D", "C;D;A", "only contains")
verify_lists("A;B;C;D", "", "only contains")
verify_lists("A;B;C;D", "A;B;C;D", "only contains")
verify_lists("A;B;C;D", "D;C;A;B", "only contains")

verify_lists("A;B;C;D", "A", "contains in order")
verify_lists("A;B;C;D", "A;B;C;D", "contains in order")
verify_lists("A;B;C", "A;B;C;D", "contains in order")
verify_lists("A;B;C;D", "A;D;C;E", "contains in order")
verify_lists("A;B;C;D", "C;D;A", "contains in order")
verify_lists("A;B;C;D", "", "contains in order")
verify_lists("A;B;C;D", "A;B;C;D", "contains in order")
verify_lists("A;B;C;D;E", "D;C;A;B", "contains in order")
